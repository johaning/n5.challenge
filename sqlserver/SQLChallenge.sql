CREATE database N5Challenge;

use N5Challenge;

IF EXISTS (SELECT * FROM sysobjects WHERE name='Permissions')
    DROP TABLE Permissions
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name='PermissionTypes')
    DROP TABLE PermissionTypes
GO

CREATE TABLE Permissions (
    Id INTEGER IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(MAX) NOT NULL,
    ApellidoEmpleado VARCHAR(MAX) NOT NULL,
    TipoPermiso INTEGER NOT NULL,
    FechaPermiso DATE NOT NULL
);

CREATE TABLE PermissionTypes (
    Id INTEGER IDENTITY(1,1) PRIMARY KEY,
    Descripcion VARCHAR(MAX) NOT NULL,
);

ALTER TABLE Permissions
ADD FOREIGN KEY (TipoPermiso) REFERENCES PermissionTypes(Id);

--QUERYS
DELETE FROM Permissions;
DELETE FROM PermissionTypes;

INSERT INTO PermissionTypes (Descripcion)
VALUES ('Administrador');
INSERT INTO PermissionTypes (Descripcion)
VALUES ('Cliente');
INSERT INTO PermissionTypes (Descripcion)
VALUES ('Visitor');

INSERT INTO Permissions (NombreEmpleado, ApellidoEmpleado, TipoPermiso, FechaPermiso)
VALUES ('Empleado 1', 'Apellido 1', (SELECT Id FROM PermissionTypes WHERE Descripcion = 'Administrador'), GETDATE());
INSERT INTO Permissions (NombreEmpleado, ApellidoEmpleado, TipoPermiso, FechaPermiso)
VALUES ('Empleado 2', 'Apellido 2', (SELECT Id FROM PermissionTypes WHERE Descripcion = 'Cliente'), GETDATE());

--SELECT * FROM Permissions
--SELECT * FROM PermissionTypes