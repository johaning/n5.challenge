﻿using System;
namespace N5Challenge.Domain.Events
{
	public class PermissionEvent
	{
		public Guid Id { get; set; }
		public string NameOperation { get; set; }
	}
}

