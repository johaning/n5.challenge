﻿namespace N5Challenge.Domain.Aggregates.PermissionType
{
    public interface IPermissionTypesRepository
    {
        Task<List<PermissionTypes>> GetPermissionTypes();
    }
}

