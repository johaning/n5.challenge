﻿namespace N5Challenge.Domain.Aggregates.Permission
{
	public interface IPermissionsRepository
	{
		Task<List<Permissions>> GetPermissions();
        Task<Permissions> RequestPermissions(Permissions permission);
        Task<Permissions> ModifyPermissions(Permissions permission);
    }
}

