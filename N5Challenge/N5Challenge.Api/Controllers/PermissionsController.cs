﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using N5Challenge.Api.Application.Commands;
using N5Challenge.Api.Application.Queries;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Controllers;

[ApiController]
[Route("[controller]/[Action]")]
public class PermissionsController : ControllerBase
{
    private readonly ILogger<PermissionsController> _logger;
    private readonly IMediator _mediator;

    public PermissionsController(
        ILogger<PermissionsController> logger,
        IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet(Name = "GetPermissions")]
    public async Task<IActionResult> GetPermissions()
    {
        var result = await _mediator.Send(new GetPermissionsQuery());
        return Ok(result);
    }

    [HttpPost(Name = "RequestPermission")]
    public async Task<IActionResult> RequestPermission(Permissions permissions)
    {
        var result = await _mediator.Send(new RequestPermissionCommand(permissions));
        return Ok(result);
    }

    [HttpPut(Name = "ModifyPermission")]
    public async Task<IActionResult> ModifyPermission(Permissions permissions)
    {
        var result = await _mediator.Send(new ModifyPermissionCommand(permissions));
        return Ok(result);
    }
}

