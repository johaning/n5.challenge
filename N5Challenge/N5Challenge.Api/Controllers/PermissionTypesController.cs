﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using N5Challenge.Api.Application.Queries;

namespace N5Challenge.Api.Controllers;

[ApiController]
[Route("[controller]/[Action]")]
public class PermissionTypesController : ControllerBase
{
    private readonly ILogger<PermissionTypesController> _logger;
    private readonly IMediator _mediator;

    public PermissionTypesController(
        ILogger<PermissionTypesController> logger,
        IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet(Name = "GetPermissionTypes")]
    public async Task<IActionResult> GetPermissionTypes()
    {
        var result = await _mediator.Send(new GetPermissionTypesQuery());
        return Ok(result);
    }
}

