﻿using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using N5Challenge.Api.Application.Consumers;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Domain.Aggregates.Permission;
using N5Challenge.Domain.Aggregates.PermissionType;
using N5Challenge.Domain.Events;
using N5Challenge.Infrastructure.Context;
using N5Challenge.Infrastructure.Repository;
using N5Challenge.Infrastructure.Services;
using Nest;
using SlimMessageBus.Host;
using SlimMessageBus.Host.Kafka;
using SlimMessageBus.Host.Serialization.Json;

var builder = WebApplication.CreateBuilder(args);

var configuration = new ConfigurationBuilder()
    .SetBasePath(builder.Environment.ContentRootPath)
    .AddJsonFile("appsettings.json")
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", true)
    .AddEnvironmentVariables()
    .Build();

builder.Services.AddSingleton<IConfiguration>(configuration);

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
    policy => policy
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowAnyOrigin()
    );
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMediatR(config =>
    config.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly())
);

//Contextdb
builder.Services.AddDbContext<PermissionsDbContext>(options =>
    options.UseSqlServer(configuration["SqlServerConnection"])
);
builder.Services.AddDbContext<PermissionTypesDbContext>(options =>
    options.UseSqlServer(configuration["SqlServerConnection"])
);

//Repositories
builder.Services.AddScoped<IPermissionsRepository, PermissionsRepository>();
builder.Services.AddScoped<IPermissionTypesRepository, PermissionTypesRepository>();
builder.Services.AddScoped<IElasticSearchService, ElasticSearchService>();

//Managers
builder.Services.AddTransient<IPermissionManager, PermissionManager>();

//kafka
builder.Services.AddSingleton<PermissionConsumer>();
builder.Services.AddSlimMessageBus(bus =>
{
    bus.AddChildBus("bus", (builder) =>
    {
        builder.Produce<PermissionEvent>(x => {
            x.DefaultTopic(configuration["kafka:topics:operation_permission"]);
            x.KeyProvider((@event, s) => Encoding.ASCII.GetBytes(@event.Id.ToString()));
        });
        builder.Consume<PermissionEvent>(x =>
        {
            x.Topic(configuration["kafka:topics:operation_permission"])
            .WithConsumer<PermissionConsumer>()
            .KafkaGroup(configuration["kafka:groups:operation_permission"]);
        });
        builder.WithProviderKafka(config =>
        {
            config.BrokerList = configuration["kafka:url"];
            config.ProducerConfig = (x) =>
            {
                x.LingerMs = 1;
                x.SocketNagleDisable = true;
            };
            config.ConsumerConfig = (x) =>
            {
                x.FetchErrorBackoffMs = 1;
                x.SocketNagleDisable = true;
            };
        })
        .AddJsonSerializer();
    });
});

//ElasticSearch
builder.Services.AddSingleton<IElasticClient>(provider =>
{
    var uri = new Uri(configuration["elasticsearch:url"]);
    var settings = new ConnectionSettings(uri).DefaultIndex(configuration["elasticsearch:defaultindex"]);
    return new ElasticClient(settings);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseRouting();
app.UseCors("CorsPolicy");
app.UseAuthorization();
app.MapControllers();
app.Run();