﻿using MediatR;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Application.Queries
{
	public class GetPermissionsQueryHandler : IRequestHandler<GetPermissionsQuery, List<Permissions>>
    {
        private readonly IPermissionManager _permissionManager;

        public GetPermissionsQueryHandler(IPermissionManager permissionManager)
        {
            _permissionManager = permissionManager;
        }

        public async Task<List<Permissions>> Handle(GetPermissionsQuery request, CancellationToken cancellationToken)
        {
            return await _permissionManager.GetPermissions();
        }
    }
}

