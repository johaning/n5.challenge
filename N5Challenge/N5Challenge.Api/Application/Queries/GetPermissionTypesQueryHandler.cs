﻿using MediatR;
using N5Challenge.Domain.Aggregates.PermissionType;

namespace N5Challenge.Api.Application.Queries
{
	public class GetPermissionTypesQueryHandler : IRequestHandler<GetPermissionTypesQuery, List<PermissionTypes>>
    {
        private readonly IPermissionTypesRepository _permissionTypesRepository;

        public GetPermissionTypesQueryHandler(IPermissionTypesRepository permissionTypesRepository)
        {
            _permissionTypesRepository = permissionTypesRepository;
        }

        public async Task<List<PermissionTypes>> Handle(GetPermissionTypesQuery request, CancellationToken cancellationToken)
        {
            return await _permissionTypesRepository.GetPermissionTypes();
        }
    }
}

