﻿
using MediatR;
using N5Challenge.Domain.Aggregates.PermissionType;

namespace N5Challenge.Api.Application.Queries
{
    public class GetPermissionTypesQuery : IRequest<List<PermissionTypes>>
    {
		public GetPermissionTypesQuery()
		{
		}
	}
}

