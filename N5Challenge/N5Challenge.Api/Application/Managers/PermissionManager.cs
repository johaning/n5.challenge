﻿using N5Challenge.Domain.Aggregates.Permission;
using N5Challenge.Domain.Events;
using N5Challenge.Infrastructure.Services;
using SlimMessageBus;

namespace N5Challenge.Api.Application.Managers
{
	public interface IPermissionManager
	{
        Task<List<Permissions>> GetPermissions();
        Task<Permissions> RequestPermissions(Permissions permissions);
        Task<Permissions> ModifyPermissions(Permissions permissions);
    }

    public class PermissionManager: IPermissionManager
	{
        private readonly IPermissionsRepository _permissionsRepository;
        private readonly IMessageBus _bus;
        private readonly IElasticSearchService _elasticSearchService;

        public enum OperationNameEnum
        {
            Get,
            Request,
            Modify
        }

        public PermissionManager(
            IPermissionsRepository permissionsRepository,
            IMessageBus bus,
            IElasticSearchService elasticSearchService
            )
        {
            _permissionsRepository = permissionsRepository;
            _bus = bus;
            _elasticSearchService = elasticSearchService;
            _elasticSearchService.CreatePermissionIndex();
        }

        public async Task<List<Permissions>> GetPermissions()
        {
            PublishPermissionMessage(OperationNameEnum.Get);

            await _elasticSearchService.GetPermissionsIndex();

            return await _permissionsRepository.GetPermissions();
        }

        public async Task<Permissions> ModifyPermissions(Permissions permissions)
        {
            PublishPermissionMessage(OperationNameEnum.Modify);
            return await _permissionsRepository.ModifyPermissions(permissions);
        }

        public async Task<Permissions> RequestPermissions(Permissions permissions)
        {
            PublishPermissionMessage(OperationNameEnum.Request);

            await _elasticSearchService.RequestPermissionIndex(permissions);

            return await _permissionsRepository.RequestPermissions(permissions);
        }

        private async Task PublishPermissionMessage(OperationNameEnum operationType)
        {
            await _bus.Publish(new PermissionEvent()
            {
                Id = Guid.NewGuid(),
                NameOperation =operationType.ToString()
            });
        }
    }
}

