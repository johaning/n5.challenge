﻿using N5Challenge.Domain.Events;
using SlimMessageBus;

namespace N5Challenge.Api.Application.Consumers
{
	public class PermissionConsumer: IConsumer<PermissionEvent>
    {
        private readonly ILogger<PermissionConsumer> _logger;
        public PermissionConsumer(ILogger<PermissionConsumer> logger)
		{
            _logger = logger;
		}

        public Task OnHandle(PermissionEvent message)
        {
            _logger.LogInformation($"Operation Permission: {message.Id} | {message.NameOperation}");
            return Task.CompletedTask;
        }
    }
}

