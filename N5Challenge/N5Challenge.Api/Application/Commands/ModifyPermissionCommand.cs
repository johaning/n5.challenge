﻿using MediatR;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Application.Commands
{
	public class ModifyPermissionCommand : IRequest<Permissions>
	{
        public Permissions permissions { get; set; }
        public ModifyPermissionCommand(Permissions permissions)
        {
            this.permissions = permissions;
        }
    }
}

