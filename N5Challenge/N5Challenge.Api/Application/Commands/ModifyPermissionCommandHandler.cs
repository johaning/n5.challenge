﻿using MediatR;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Application.Commands
{
	public class ModifyPermissionCommandHandler : IRequestHandler<ModifyPermissionCommand, Permissions>
	{
        private readonly IPermissionManager _permissionManager;

        public ModifyPermissionCommandHandler(IPermissionManager permissionsManager)
        {
            _permissionManager = permissionsManager;
        }

        public async Task<Permissions> Handle(ModifyPermissionCommand request, CancellationToken cancellationToken)
        {
            return await _permissionManager.ModifyPermissions(request.permissions);
        }
    }
}

