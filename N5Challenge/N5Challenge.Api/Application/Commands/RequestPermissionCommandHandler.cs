﻿using MediatR;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Application.Commands
{
    public class RequestPermissionCommandHandler : IRequestHandler<RequestPermissionCommand, Permissions>
    {
        private readonly IPermissionManager _permissionManager;

        public RequestPermissionCommandHandler(IPermissionManager permissionsManager)
        {
            _permissionManager = permissionsManager;
        }

        public async Task<Permissions> Handle(RequestPermissionCommand request, CancellationToken cancellationToken)
        {
            return await _permissionManager.RequestPermissions(request.permissions);
        }
    }
}

