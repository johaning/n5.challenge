﻿using MediatR;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Application.Commands
{
    public class RequestPermissionCommand : IRequest<Permissions>
    {
		public Permissions permissions { get; set; }
		public RequestPermissionCommand(Permissions permissions)
		{
			this.permissions = permissions;
		}
	}
}

