﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using N5Challenge.Api.Application.Commands;
using N5Challenge.Api.Application.Queries;
using N5Challenge.Api.Controllers;
using N5Challenge.Api.Test.Stubs;
using N5Challenge.Domain.Aggregates.Permission;
using Xunit;

namespace N5Challenge.Api.Test.Controllers
{
	public class PermissionsControllerTest
	{

        private readonly Mock<IMediator> _mediatorMock;
        private readonly  Mock<ILogger<PermissionsController>> _loggerMock;
        private PermissionsController _permissionsController;

        public PermissionsControllerTest()
        {
            _mediatorMock = new Mock<IMediator>();
            _loggerMock = new Mock<ILogger<PermissionsController>>();
            _permissionsController = new PermissionsController(_loggerMock.Object, _mediatorMock.Object);
        }

        [Fact]
        public async Task GetPermissions_ReturnsOk()
        {
            _ = _mediatorMock
                .Setup(x => x.Send(It.IsAny<GetPermissionsQuery>(), default(CancellationToken)))
                .ReturnsAsync(PermissionStubs.GetPermissions());

            var result = await _permissionsController.GetPermissions();

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult.Value);
        }

        [Fact]
        public async void RequestPermission_ReturnsOk()
        {
            _ = _mediatorMock
                .Setup(x => x.Send(It.IsAny<RequestPermissionCommand>(), default(CancellationToken)))
                .ReturnsAsync(PermissionStubs.GetPermission());

            var result = await _permissionsController.RequestPermission(new Permissions());

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult.Value);
        }

        [Fact]
        public async void ModifyPermission_ReturnsOk()
        {
            _ = _mediatorMock
                .Setup(x => x.Send(It.IsAny<ModifyPermissionCommand>(), default(CancellationToken)))
                .ReturnsAsync(PermissionStubs.GetPermission());

            var result = await _permissionsController.ModifyPermission(new Permissions());

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult.Value);
        }

    }
}

