﻿using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Api.Test.Stubs
{
	public class PermissionStubs
	{
		public static List<Permissions> GetPermissions()
		{
			return new()
			{
				GetPermission()
			};
		}

        public static Permissions GetPermission()
        {
			return new()
			{
				NombreEmpleado = "empleado 1",
				ApellidoEmpleado = "apellido 1",
				TipoPermiso = 1,
				FechaPermiso = DateTime.Now,
			};
        }
    }
}

