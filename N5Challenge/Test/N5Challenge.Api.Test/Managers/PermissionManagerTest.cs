﻿using Moq;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Api.Test.Stubs;
using N5Challenge.Domain.Aggregates.Permission;
using N5Challenge.Infrastructure.Services;
using SlimMessageBus;
using Xunit;

namespace N5Challenge.Api.Test.Managers
{
	public class PermissionManagerTest
	{
        private readonly Mock<IPermissionsRepository> _permissionsRepositoryMock;
        private readonly Mock<IMessageBus> _busMock;
        private readonly Mock<IElasticSearchService> _elasticsearch;
        private PermissionManager _permissionManager;

        public PermissionManagerTest()
		{
            _permissionsRepositoryMock = new Mock<IPermissionsRepository>();
            _busMock = new Mock<IMessageBus>();
            _elasticsearch = new Mock<IElasticSearchService>();
            _permissionManager = new PermissionManager(
                _permissionsRepositoryMock.Object,
                _busMock.Object,
                _elasticsearch.Object);
        }

        [Fact]
        public async Task GetPermissions_ReturnsPermissions()
        {
            _permissionsRepositoryMock.Setup(repo => repo.GetPermissions())
            .ReturnsAsync(PermissionStubs.GetPermissions());

            var result = await _permissionManager.GetPermissions();

            Assert.NotNull(result);
            Assert.IsType<List<Permissions>>(result);
        }

        [Fact]
        public async Task RequestPermissions_ReturnsRegisteredPermission()
        {
            _permissionsRepositoryMock.Setup(repo => repo.RequestPermissions(It.IsAny<Permissions>()))
                .ReturnsAsync((Permissions registeredPermission) => registeredPermission);

            var registeredPermission = await _permissionManager.RequestPermissions(new Permissions());

            Assert.NotNull(registeredPermission);
        }

        [Fact]
        public async Task ModifyPermissions_ReturnsModifiedPermission()
        {
            _permissionsRepositoryMock.Setup(repo => repo.ModifyPermissions(It.IsAny<Permissions>()))
                .ReturnsAsync((Permissions modifiedPermission) => modifiedPermission);

            var modifiedPermission = await _permissionManager.ModifyPermissions(new Permissions());

            Assert.NotNull(modifiedPermission);
        }
    }
}

