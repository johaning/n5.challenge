﻿using Microsoft.Extensions.Logging;
using Moq;
using N5Challenge.Api.Application.Consumers;
using N5Challenge.Domain.Events;
using Xunit;

namespace N5Challenge.Api.Test.Consumers
{
	public class PermissionConsumerTest
	{
        [Fact]
        public async Task OnHandle_OperationPermission()
        {
            var loggerMock = new Mock<ILogger<PermissionConsumer>>();
            var permissionConsumer = new PermissionConsumer(loggerMock.Object);
            var permissionEvent = new PermissionEvent
            {
                Id = Guid.NewGuid(),
                NameOperation = "request"
            };

            await permissionConsumer.OnHandle(permissionEvent);
        }
    }
}

