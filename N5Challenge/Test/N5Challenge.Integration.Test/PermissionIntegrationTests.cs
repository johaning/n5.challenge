﻿using System.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using N5Challenge.Api.Application.Consumers;
using N5Challenge.Api.Application.Managers;
using N5Challenge.Domain.Aggregates.Permission;
using N5Challenge.Domain.Events;
using N5Challenge.Infrastructure.Context;
using N5Challenge.Infrastructure.Repository;
using N5Challenge.Infrastructure.Services;
using SlimMessageBus;
using Xunit;

namespace N5Challenge.Integration.Test
{
	public class PermissionIntegrationTests : IDisposable
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly PermissionsDbContext _context;

        public PermissionIntegrationTests()
        {
            var services = new ServiceCollection();
            services.AddDbContext<PermissionsDbContext>(options => options.UseInMemoryDatabase("InMemoryDatabase"));
            services.AddTransient<IPermissionsRepository, PermissionsRepository>();
            services.AddTransient<IPermissionManager, PermissionManager>();
            services.AddTransient<IElasticSearchService, ElasticSearchService>();

            var messageBusMock = new Mock<IMessageBus>();
            messageBusMock.Setup(mb => mb.Publish(
                It.IsAny<PermissionEvent>(),
                It.IsAny<string>(),
                It.IsAny<IDictionary<string, object>>(),
                default(CancellationToken)))
            .Verifiable();

            var elasticSearch = new Mock<IElasticSearchService>();
            elasticSearch.Setup(el => el.CreatePermissionIndex()).Verifiable();

            services.AddSingleton<IMessageBus>(messageBusMock.Object);

            _serviceProvider = services.BuildServiceProvider();
            _context = _serviceProvider.GetRequiredService<PermissionsDbContext>();
            _context.Database.EnsureDeleted();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        [Fact]
        public async Task PermissionManager_GetPermissions_IntegrationTest()
        {
            var permissionManager = _serviceProvider.GetRequiredService<IPermissionManager>();

            await permissionManager.GetPermissions();

            var permissions = await _context.Permissions.ToListAsync();
            Assert.NotNull(permissions);
        }

        [Fact]
        public async Task PermissionManager_RequestPermissions_IntegrationTest()
        {
            var permissionManager = _serviceProvider.GetRequiredService<IPermissionManager>();
            var permissions = new Permissions
            {
                NombreEmpleado = "empelado 1",
                ApellidoEmpleado = "apellido 1",
                TipoPermiso = 1001,
                FechaPermiso = DateTime.Now,
            };
            await permissionManager.RequestPermissions(permissions);

            _context.Permissions.Add(permissions);
            await _context.SaveChangesAsync();
            Assert.NotNull(permissions);
        }

        [Fact]
        public async Task PermissionManager_ModifyPermissions_IntegrationTest()
        {
            var permissionManager = _serviceProvider.GetRequiredService<IPermissionManager>();
            var permissions = new Permissions
            {
                Id = 123,
                NombreEmpleado = "empelado 1",
                ApellidoEmpleado = "apellido 1",
                TipoPermiso = 1001,
                FechaPermiso = DateTime.Now,
            };

            await permissionManager.ModifyPermissions(permissions);

            _context.Entry(permissions).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            Assert.NotNull(permissions);
        }

    }

}

