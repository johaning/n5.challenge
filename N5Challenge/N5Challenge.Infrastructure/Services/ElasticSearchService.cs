﻿using System;
using System.Reflection.Metadata;
using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N5Challenge.Domain.Aggregates.Permission;
using Nest;

namespace N5Challenge.Infrastructure.Services
{
	public interface IElasticSearchService
    {
        void CreatePermissionIndex();
        Task RequestPermissionIndex(Permissions permission);
        Task ModifyPermissionIndex(Permissions permissions, int id);
        Task<List<Permissions>> GetPermissionsIndex();
    }

    public class ElasticSearchService : IElasticSearchService
    {
        private readonly IElasticClient _elasticClient;
        private readonly IConfiguration _configuration;
        private readonly ILogger<ElasticSearchService> _logger;

        public ElasticSearchService(
            IElasticClient elasticClient,
            IConfiguration configuration,
            ILogger<ElasticSearchService> logger)
        {
            _elasticClient = elasticClient;
            _configuration = configuration;
            _logger = logger;
        }

        public void CreatePermissionIndex()
        {
            try
            {
                var indexResponse = _elasticClient.Indices
                .CreateAsync(_configuration["elasticsearch:defaultindex"], index => index
                .Map<Permissions>(m => m.AutoMap()));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }

        public async Task RequestPermissionIndex(Permissions permission)
        {
            try
            {
                await _elasticClient.IndexAsync(permission, idx => idx.Index(_configuration["elasticsearch:defaultindex"])
                .OpType(OpType.Index));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }

        public async Task ModifyPermissionIndex(Permissions permission, int id)
        {
            try
            {
                var updateResponse = await _elasticClient.UpdateAsync<Permissions, object>(
                new DocumentPath<Permissions>(id),
                u => u.Doc(permission));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }

        public async Task<List<Permissions>> GetPermissionsIndex()
        {
            try
            {
                var searchResponse = await _elasticClient.SearchAsync<Permissions>(search => search.MatchAll());
                if (searchResponse.IsValid)
                {
                    return searchResponse.Documents.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return new();
        }
    }
}

