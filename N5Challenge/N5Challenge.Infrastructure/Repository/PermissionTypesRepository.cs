﻿using Microsoft.EntityFrameworkCore;
using N5Challenge.Domain.Aggregates.PermissionType;
using N5Challenge.Infrastructure.Context;

namespace N5Challenge.Infrastructure.Repository
{
	public class PermissionTypesRepository : IPermissionTypesRepository
	{
        private readonly PermissionTypesDbContext _context;

        public PermissionTypesRepository(PermissionTypesDbContext context)
        {
            _context = context;
        }

        public async Task<List<PermissionTypes>> GetPermissionTypes()
        {
            return await _context.PermissionTypes.ToListAsync();
        }
    }
}

