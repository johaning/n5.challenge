﻿using Microsoft.EntityFrameworkCore;
using N5Challenge.Domain.Aggregates.Permission;
using N5Challenge.Infrastructure.Context;

namespace N5Challenge.Infrastructure.Repository
{
    public class PermissionsRepository : IPermissionsRepository
    {
        private readonly PermissionsDbContext _context;

        public PermissionsRepository(PermissionsDbContext context)
        {
            _context = context;
        }

        public async Task<List<Permissions>> GetPermissions()
        {
            return await _context.Permissions.ToListAsync();
        }

        public async Task<Permissions> ModifyPermissions(Permissions permission)
        {
            _context.Entry(permission).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return permission;
        }

        public async Task<Permissions> RequestPermissions(Permissions permission)
        {
            _context.Permissions.Add(permission);
            await _context.SaveChangesAsync();
            return permission;
        }
    }
}

