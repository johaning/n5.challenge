﻿using System;
using Microsoft.EntityFrameworkCore;
using N5Challenge.Domain.Aggregates.Permission;

namespace N5Challenge.Infrastructure.Context
{
	public class PermissionsDbContext : DbContext
	{
        public PermissionsDbContext(DbContextOptions<PermissionsDbContext> options) : base(options) { }

        public DbSet<Permissions> Permissions { get; set; }
    }
}

