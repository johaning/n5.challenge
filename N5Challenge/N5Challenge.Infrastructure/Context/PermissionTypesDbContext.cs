﻿using System;
using Microsoft.EntityFrameworkCore;
using N5Challenge.Domain.Aggregates.PermissionType;

namespace N5Challenge.Infrastructure.Context
{
	public class PermissionTypesDbContext : DbContext
	{
        public PermissionTypesDbContext(DbContextOptions<PermissionTypesDbContext> options) : base(options) { }

        public DbSet<PermissionTypes> PermissionTypes { get; set; }
    }
}

