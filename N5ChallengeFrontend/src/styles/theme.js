// themes.js
import { createTheme } from '@mui/material/styles';
import { 
    lightGreen, 
    green,
    blue,
    deepPurple
 } from '@mui/material/colors';

const greenTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: green,
  },
});

const blueTheme = createTheme({
    palette: {
        primary: blue,
        secondary: deepPurple,
    },
});

export { greenTheme, blueTheme };
