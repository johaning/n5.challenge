import axios from 'axios';

const get = (url, params) => {
    axios.defaults.baseURL = 'http://localhost:5000/';
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    return new Promise((resolve, reject) =>{
        axios
        .get(url, params)
        .then((res) => resolve(res.data))
        .catch((err) => {
            reject(err)
        });
    });
};

const post = (url, data) => {
    axios.defaults.baseURL = 'http://localhost:5000/';
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    return new Promise((resolve, reject) =>{
        axios
        .post(url, data)
        .then((res) => {
            resolve(res.data)
        })
        .catch((err) =>{
            reject(err);
        });
    });
};

const put = (url, data) => {
    axios.defaults.baseURL = 'http://localhost:5000/';
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    return new Promise((resolve, reject) =>{
        axios
        .put(url, data)
        .then((res) => {
            resolve(res.data)
        })
        .catch((err) =>{
            reject(err);
        });
    });
};

export default{
    get,
    post,
    put
};

