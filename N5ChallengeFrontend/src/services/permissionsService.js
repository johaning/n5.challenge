import {httpclient} from './index';

const getPermissions = async () => {
    return await httpclient.get('Permissions/GetPermissions')
    .catch(err =>{
        return [];
    })
}

const requestPermission = async (permission) => {
    return await httpclient.post('Permissions/RequestPermission', permission)
    .catch(err =>{
        return null;
    })
}

const modifyPermission = async (permission) => {
    return await httpclient.put('Permissions/ModifyPermission', permission)
    .catch(err =>{
        return  null;
    })
}

export default{
    getPermissions,
    requestPermission,
    modifyPermission
};

