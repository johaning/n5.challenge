import {httpclient} from './index';

const getPermissionTypes = async () => {
    return await httpclient.get('PermissionTypes/GetPermissionTypes')
    .catch(err =>{
        return [];
    })
}

export default{
    getPermissionTypes
};

