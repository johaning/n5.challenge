import httpclient from "./providers/httpclient";
import permissionsService from "./permissionsService";
import permissionTypesService from "./permissionTypesService";

export{
    httpclient,
    permissionsService,
    permissionTypesService
};
