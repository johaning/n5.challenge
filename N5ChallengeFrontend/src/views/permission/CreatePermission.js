import { useEffect, useState } from 'react';
import { 
    TextField, 
    Button, 
    Grid, 
    MenuItem,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle
} from '@mui/material';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { 
    permissionsService, 
    permissionTypesService 
} from '../../services';
import { format } from 'date-fns';

const initialForm = {
    nombreEmpleado: '',
    apellidoEmpleado: '',
    tipoPermiso: '',
    fechaPermiso: new Date(),
};

export default function CreatePermission(props)
{
    const [formData, setFormData] = useState(initialForm);
    const [permissionTypes, setPermissionTypes] = useState([]);

    useEffect(() =>{
        loadPermissionTypes();
        if (props.modifyPermission) {
            const {modifyPermission} = props;
            setFormData({
                nombreEmpleado: modifyPermission.nombreEmpleado,
                apellidoEmpleado: modifyPermission.apellidoEmpleado,
                tipoPermiso: modifyPermission.tipoPermiso,
                fechaPermiso: new Date(modifyPermission.fechaPermiso),
            });
            return;
        }
        setFormData(initialForm);
    },[props.modifyPermission])

    const loadPermissionTypes = async () => {
        const permissionTypesData = await permissionTypesService.getPermissionTypes();
        if(!permissionTypesData) return;
        setPermissionTypes(permissionTypesData);
    }

    const closeDialog = () => {
        props.isOpen(false);
    };

    const onChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };
    
    const onChangeDate = (date) => {
        setFormData({ ...formData, fechaPermiso: date });
    };

    const handlePermission = async (e) => {
        e.preventDefault();
        if(props.modifyPermission){
            await modifyPermission();
        }else{
            await newPermission();
        }
        closeDialog();
        props.refresh();
    };

    const modifyPermission = async () => {
        await permissionsService.modifyPermission({
            id: props.modifyPermission.id,
            nombreEmpleado: formData.nombreEmpleado,
            apellidoEmpleado: formData.apellidoEmpleado,
            tipoPermiso: formData.tipoPermiso,
            fechaPermiso: format(formData.fechaPermiso, 'yyyy-MM-dd'),
        });
    }

    const newPermission = async () => {
        await permissionsService.requestPermission({
            nombreEmpleado: formData.nombreEmpleado,
            apellidoEmpleado: formData.apellidoEmpleado,
            tipoPermiso: formData.tipoPermiso,
            fechaPermiso: format(formData.fechaPermiso, 'yyyy-MM-dd'),
        });
    }

    const rederForm = () => {
        return(
            <form>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Nombre del Empleado"
                            variant="outlined"
                            name="nombreEmpleado"
                            value={formData.nombreEmpleado}
                            onChange={onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            label="Apellido del Empleado"
                            variant="outlined"
                            name="apellidoEmpleado"
                            value={formData.apellidoEmpleado}
                            onChange={onChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            label="Tipo de Permiso"
                            variant="outlined"
                            name="tipoPermiso"
                            select
                            value={formData.tipoPermiso}
                            onChange={onChange}
                        >
                            {permissionTypes.map((item) =>
                            (
                                <MenuItem key={item.id} value={item.id}>{item.descripcion}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={6}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker 
                                fullWidth
                                label="Fecha de Permiso"
                                inputVariant="outlined"
                                name="fechaPermiso"
                                value={formData.fechaPermiso}
                                onChange={onChangeDate}
                            />
                        </LocalizationProvider>
                    </Grid>
                </Grid>
            </form>
        );
    }

    return(
        <Dialog open={props.open} onClose={closeDialog}>
            <DialogTitle>Crear permiso</DialogTitle>
                <DialogContent>
                    {rederForm()}
                </DialogContent>
            <DialogActions>
                <Button
                    size="small"
                    onClick={closeDialog}>
                    Cancelar
                </Button>
                <Button
                    size="small"
                    variant="contained"
                    color="secondary"
                    onClick={handlePermission}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>
    );
}