import { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { permissionsService } from '../../services';
import { CreatePermission} from '../index';

export default function Permissions()
{
    const [permissions, setPermissions] = useState([]);
    const [modifyPermission, setModifyPermission] = useState(undefined);
    const [openDialog, isOpenDialog] = useState(false);

    useEffect(() =>{
        load();
    },[])

    const load = async () => {
        const permissionsData = await permissionsService.getPermissions();
        if(!permissionsData) return;
        setPermissions(permissionsData);
    }

    const refresh = () => {
        load();
    }

    const onCreate = (permission = undefined) => {
        setModifyPermission(permission);
        isOpenDialog(true);
    }

    return(
        <>
            <Container maxWidth="md">
                <div>
                    <Button 
                        size="small"
                        color="secondary"
                        variant="contained"
                        startIcon={<AddCircleIcon/>}
                        onClick={ () => onCreate() }>
                        Crear Permiso
                    </Button>
                </div>
                <h3>Permisos</h3>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align='center' sx={{fontWeight: 'bold'}}>Nombre</TableCell>
                                <TableCell align="center" sx={{fontWeight: 'bold'}}>Apellido</TableCell>
                                <TableCell align="center" sx={{fontWeight: 'bold'}}>Tipo Permiso</TableCell>
                                <TableCell align="center" sx={{fontWeight: 'bold'}}>Fecha Permiso</TableCell>
                                <TableCell align="center" sx={{fontWeight: 'bold'}}>Acción</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {permissions.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell align="left">{row.nombreEmpleado}</TableCell>
                                    <TableCell align="left">{row.apellidoEmpleado}</TableCell>
                                    <TableCell align="center">{row.tipoPermiso}</TableCell>
                                    <TableCell align="center">{row.fechaPermiso}</TableCell>
                                    <TableCell align="center">
                                        <Button 
                                            size="small" 
                                            variant="outlined"
                                            startIcon={<EditIcon/>}
                                            onClick={ () =>onCreate(row)}>
                                            Editar
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
            <CreatePermission 
                open={openDialog} 
                isOpen = {isOpenDialog}
                refresh = {refresh}
                modifyPermission = {modifyPermission}/>
        </>
    );
}