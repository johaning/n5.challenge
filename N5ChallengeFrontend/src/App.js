import './App.css';
import { useEffect, useState } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { greenTheme, blueTheme } from './styles/theme';
import Switch from '@mui/material/Switch';
import { 
  Permissions
 } from './views/index';

function App() {

  const [theme, setTheme] = useState(blueTheme);
  const [checked, setChecked] = useState(false);

  const onChangeTheme = (event) => {
    const checked = event.target.checked
    setTheme(checked ? greenTheme : blueTheme)
    setChecked(checked);
  };

  return (
    <ThemeProvider theme={theme}>
      <div style={{padding: '10px'}}>
        <Switch
          
          checked={checked}
          onChange={onChangeTheme}
          inputProps={{ 'aria-label': 'controlled' }}
        />Cambiar estilo
      </div>
      <Permissions/>
    </ThemeProvider>
  );
}

export default App;
